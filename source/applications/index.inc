<div class="container-fluid">
  <div class="row">
    <div class="col-sm-12 col-xs-12 col-md-6 col-lg-6">
      <a href="https://launchpad.net/ascot" rel="nofollow"><h4 align="center"><strong>ASCoT &mdash; Automated Stability Condition Tester</strong></h4></a>
      <div class="row" align="center">
        <div class="image-cropper">
          <a href="https://launchpad.net/ascot" rel="nofollow"><img class="rounded" src='/_static/images/ascot.png' align="center"></img></a>
        </div>
      </div>
      <div class="center-justified">
        ASCoT is a light-weight, FEniCS-based, Python module
            designed for examining and determining the numerical
            stability of finite element discretizations. Its
            functionality allows for identifying stable mixed finite
            element spaces for variational saddle point problems, for
            optimizing stabilization constants, and in general, for
            computing stability constants through series of eigenvalue
            problems.
      </div>      
    </div>

    <div class="col-sm-12 col-xs-12 col-md-6 col-lg-6">
      <a href="https://bitbucket.org/fenics-apps/cbc.block" rel="nofollow"><h4 align="center"><strong>CBC.Block &mdash; Block linear algebra in Python</strong></h4></a>
      <div class="row" align="center">
        <div class="image-cropper">
          <a href="https://bitbucket.org/fenics-apps/cbc.block" rel="nofollow"><img class="rounded" src='/_static/images/cbc_block.png' align="center"></img></a>
        </div>
      </div>
      <div class="center-justified">
        CBC.PDESys is a Python package for specifying and solving
              large, completely general, systems of nonlinear Partial Differential
              Equations (PDEs) with very compact, flexible and reusable code.
              CBC.CFD is a subpackage of CBC.PDESys that focuses on Computational
              Fluid Dynamics and turbulence modeling. CBC.CFD also contains some highly
              optimized Navier-Stokes solvers.
      </div>
    </div>
  </div>


  <div class="row">
    <div class="col-sm-12 col-xs-12 col-md-6 col-lg-6">
       <a href="https://launchpad.net/cbcpdesys" rel="nofollow"><h4 align="center"><strong>CBC.PDESys &mdash; Specify large systems of PDEs with ease</strong></h4></a>
      <div class="row" align="center">
        <div class="image-cropper">
          <a href="https://launchpad.net/cbcpdesys" rel="nofollow"><img class="rounded" src='/_static/images/featured/featured_item_pdesys.png' align="center"></img></a>
        </div>
      </div>
      <div class="center-justified">
          CBC.PDESys is a Python package for specifying and solving
            large, completely general, systems of nonlinear Partial Differential
            Equations (PDEs) with very compact, flexible and reusable code.
            CBC.CFD is a subpackage of CBC.PDESys that focuses on Computational
            Fluid Dynamics and turbulence modeling. CBC.CFD also contains some highly
            optimized Navier-Stokes solvers.
      </div>      
    </div>

    <div class="col-sm-12 col-xs-12 col-md-6 col-lg-6">
      <a href="https://launchpad.net/cbc.solve" rel="nofollow"><h4 align="center"><strong>CBC.Solve &mdash; A collection of Python solvers</strong></h4></a>
      <div class="row" align="center">
        <div class="image-cropper">
          <a href="https://launchpad.net/cbc.solve" rel="nofollow"><img class="rounded" src='/_static/images/cbc_solve.png' align="center"></img></a>
        </div>
      </div>
      <div class="center-justified">
        CBC.Solve is a collection of FEniCS based solvers
         developed at the <a href="http://cbc.simula.no">Center
         for Biomedical Computing</a> hosted
         by <a href="http://www.simula.no">Simula Research
         Laboratory</a> in Oslo. The current collection of solvers
         includes CBC.Flow (Navier-Stokes), CBC.Twist
         (hyperelasticity), CBC.Swing (fluid-structure
         interaction) and CBC.Beat (bidomain equations).
      </div>
    </div>
  </div>


  <div class="row">
    <div class="col-sm-12 col-xs-12 col-md-6 col-lg-6">
      <a href="http://www.dolfin-adjoint.org" rel="nofollow"><h4 align="center"><strong>dolfin-adjoint &mdash; A library for adjoining models</strong></h4></a>
      <div class="row" align="center">
        <div class="image-cropper">
          <a href="http://www.dolfin-adjoint.org" rel="nofollow"><img class="rounded" src='/_static/images/dolfinadjoint_tmp.png' align="center"></img></a>
        </div>
      </div>
      <div class="center-justified">
        The main aim of dolfin-adjoint is to facilitate the
            automated development of reliable adjoint models. This
            Python library fits seamlessly on top of the FEniCS core
            components and offers easy-to-use functionality for
            automatically deriving and efficiently solving adjoint and
            tangent linear models starting from DOLFIN-based forward
            models. 
      </div>      
    </div>

    <div class="col-sm-12 col-xs-12 col-md-6 col-lg-6">
     <a href="https://launchpad.net/cbc.solve" rel="nofollow"><h4 align="center"><strong>DOLFWAVE &mdash; A library for surface water waves</strong></h4></a>
      <div class="row" align="center">
        <div class="image-cropper">
          <a href="https://launchpad.net/cbc.solve" rel="nofollow"><img class="rounded" src='/_static/images/dolfwave_tsunami.png' align="center"></img></a>
        </div>
      </div>
      <div class="center-justified">
        DOLFWAVE is a FEniCS based  library for solving surface water waves
            problems such as Tsunami generation and propagation.
            It is developed at <a href="http://ptmat.fc.ul.pt/lab_analise_num.html">
            LAN </a> hosted by <a href="http://ptmat.fc.ul.pt"> CMAF </a> in Lisbon.
            The main goal of DOLFWAVE is to provide a framework for the analysis,
            development and computation of Boussinesq-type models.
      </div>
    </div>
  </div>

  <div class="row">
    <div class="col-sm-12 col-xs-12 col-md-6 col-lg-6">
      <a href="https://bitbucket.org/fenics-apps/fenics-solid-mechanics" rel="nofollow"><h4 align="center"><strong>FEniCS Solid Mechanics &mdash; A solid mechanics library for FEniCS</strong></h4></a>
      <div class="row" align="center">
        <div class="image-cropper">
          <a href="https://bitbucket.org/fenics-apps/fenics-solid-mechanics" rel="nofollow"><img class="rounded" src='/_static/images/fenics_plasticity.png' align="center"></img></a>
        </div>
      </div>
      <div class="center-justified">
        FEniCS Solid Mechanics aims at providing functionality to make it
        simpler for application developers to create solvers for complex solid
          mechanics problems such as plasticity. Currently, the Von Mises and
          Drucker-Prager plasticity models are implemented. However, the
          structure of the library should be general enough to allow other models
          to be added with little effort.
      </div>      
    </div>

    <div class="col-sm-12 col-xs-12 col-md-6 col-lg-6">
      <a href="http://www.mathematik.uni-wuerzburg.de/~schmidt/femorph" rel="nofollow"><h4 align="center"><strong>FEMorph &mdash; A FEniCS add-on for shape optimization</strong></h4></a>
      <div class="row" align="center">
        <div class="image-cropper">
          <a href="http://www.mathematik.uni-wuerzburg.de/~schmidt/femorph" rel="nofollow"><img class="rounded" src='/_static/images/femorph.png' align="center"></img></a>
        </div>
      </div>
      <div class="center-justified">
          FEMorph is an extensive addon for Python FEniCS for large
            scale nodal shape optimization, morphing and deformation. It
            features robust grid deformation algorithms, mesh repair
            techniques, equal distant point spacing, curvature
            computation and symmetry computations, among others.
      </div>
    </div>
  </div>

  <div class="row">
    <div class="col-sm-12 col-xs-12 col-md-6 col-lg-6">
      <a href="https://launchpad.net/genfoo" rel="nofollow"><h4 align="center"><strong>GenFoo &mdash; A general Fokker-Planck solver</strong></h4></a>
      <div class="row" align="center">
        <div class="image-cropper">
          <a href="https://launchpad.net/genfoo" rel="nofollow"><img class="rounded" src='/_static/images/gen_foo_rf_heated_ions.png' align="center"></img></a>
        </div>
      </div>
      <div class="center-justified">
          GenFoo is a general Fokker-Planck solver for problems of arbitrary
            dimensionality, developed at <a href="http://www.kth.se">KTH</a>.
            It contains three backend solvers, a delta-f Monte Carlo, a standard
            Monte Carlo and a Finite Element solver.
            The key property of the GenFoo package is that physics is separated
            from numerics by runtime loading of the Fokker-Planck coefficients,
            which enable solutions of a large class of Fokker-Planck models.
      </div>      
    </div>

    <div class="col-sm-12 col-xs-12 col-md-6 col-lg-6">
      <a href="https://bitbucket.org/knutesk/gryphonproject" rel="nofollow"><h4 align="center"><strong>Gryphon &mdash; A time integrator for FEniCS</strong></h4></a>
      <div class="row" align="center">
        <div class="image-cropper">
          <a href="https://bitbucket.org/knutesk/gryphonproject" rel="nofollow"><img class="rounded" src='/_static/images/gryphon.png' align="center"></img></a>
        </div>
      </div>
      <div class="center-justified">
          Gryphon is a Python module for solving systems of time
            dependent partial differential equations in FEniCS. Currently
            supported time-stepping methods include singly
            diagonally implicit Runge-Kutta methods with an explicit first stage
            (ESDIRKs) of order 2, 3 and 4, developed at the
            Norwegian University of Science and Technology. Several examples
            are included to illustrate how to get started with solving
            time dependent problems.
      </div>
    </div>
  </div>

  <div class="row">
    <div class="col-sm-12 col-xs-12 col-md-6 col-lg-6">
      <a href="http://micromagnetics.org/magnum.fe/" rel="nofollow"><h4 align="center"><strong>magnum.fe &mdash; FEM for micromagnetics</strong></h4></a>
      <div class="row" align="center">
        <div class="image-cropper">
          <a href="http://micromagnetics.org/magnum.fe/" rel="nofollow"><img class="rounded" src='/_static/images/magnumfe.png' align="center"></img></a>
        </div>
      </div>
      <div class="center-justified">
          magnum.fe is a finite-element package for the solution of
            dynamical micromagnetic problems. It is written in Python
            and C++ and largely relies on the multi purpose
            finite-element library FEniCS. For the solution of
            open-boundary problems a shell-transformation method and a
            hybrid FEM/BEM method are implemented. The latter uses the
            open-source BEM library <a href="http://www.bempp.org/">BEM++</a>.
            magnum.fe is free software and can be extended to your needs.
      </div>      
    </div>

    <div class="col-sm-12 col-xs-12 col-md-6 col-lg-6">
      <h4 align="center"><strong>PUM solver &mdash; Partition of unity method in FEniCS</strong></h4>
      <div class="row" align="center">
        <div class="image-cropper">
          <img class="rounded" src='/_static/images/crack_dolfin.png' align="center"></img>
        </div>
      </div>
      <div class="center-justified">
          PUM solver is a FEniCS based package to model problems whose solutions
            are discontinuous across arbitrary surfaces using the partition of unity
            method (extended finite element method).
            The PUM solver consists of two components: a
            <a href="https://launchpad.net/dolfin-pum" rel="nofollow">PUM library</a>
            and a
            <a href="https://launchpad.net/ffc-pum" rel="nofollow">PUM compiler</a>.
      </div>
    </div>
  </div>

  <div class="row">
    <div class="col-sm-12 col-xs-12 col-md-6 col-lg-6">
    <a href="https://launchpad.net/unicorn" rel="nofollow"><h4 align="center"><strong>Unicorn &mdash; A massively parallel adaptive continuum solver</strong></h4></a>
      <div class="row" align="center">
        <div class="image-cropper">
          <a href="https://launchpad.net/unicorn" rel="nofollow"><img class="rounded" src='/_static/images/unicorn_mixer.gif' align="center"></img></a>
        </div>
      </div>
      <div class="center-justified">
          Unicorn is a massively parallel adaptive finite element
            solver for fluid and structure mechanics, including FSI problems.
            Unicorn aims to be a unified continuum
            mechanics solver for a wide range of applications.
            Strong scaling has been demonstrated up to
            6000 cores. It is primarily developed by <a href="http://www.csc.kth.se/ctl">CTL</a>
            hosted at <a href="http://www.kth.se">KTH</a> in Stockholm.
      </div>
    </div>
  </div>
</div>

<div class="col-sm-12 col-xs-12 col-md-12 col-lg-12" style="height:20px;"></div>